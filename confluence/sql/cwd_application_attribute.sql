INSERT INTO confluence5.dbo.cwd_application_attribute (application_id, attribute_name, attribute_value) VALUES (98305, 'aggregateMemberships', 'true');
INSERT INTO confluence5.dbo.cwd_application_attribute (application_id, attribute_name, attribute_value) VALUES (98305, 'atlassian_sha1_applied', 'true');
INSERT INTO confluence5.dbo.cwd_application_attribute (application_id, attribute_name, attribute_value) VALUES (98305, 'com.sun.jndi.ldap.connect.pool.authentication', 'simple');
INSERT INTO confluence5.dbo.cwd_application_attribute (application_id, attribute_name, attribute_value) VALUES (98305, 'com.sun.jndi.ldap.connect.pool.initsize', '1');
INSERT INTO confluence5.dbo.cwd_application_attribute (application_id, attribute_name, attribute_value) VALUES (98305, 'com.sun.jndi.ldap.connect.pool.maxsize', '0');
INSERT INTO confluence5.dbo.cwd_application_attribute (application_id, attribute_name, attribute_value) VALUES (98305, 'com.sun.jndi.ldap.connect.pool.prefsize', '10');
INSERT INTO confluence5.dbo.cwd_application_attribute (application_id, attribute_name, attribute_value) VALUES (98305, 'com.sun.jndi.ldap.connect.pool.protocol', 'plain ssl');
INSERT INTO confluence5.dbo.cwd_application_attribute (application_id, attribute_name, attribute_value) VALUES (98305, 'com.sun.jndi.ldap.connect.pool.timeout', '30000');