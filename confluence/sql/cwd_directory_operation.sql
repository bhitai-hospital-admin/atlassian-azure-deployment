INSERT INTO confluence5.dbo.cwd_directory_operation (directory_id, operation_type) VALUES (131073, 'CREATE_GROUP');
INSERT INTO confluence5.dbo.cwd_directory_operation (directory_id, operation_type) VALUES (131073, 'CREATE_ROLE');
INSERT INTO confluence5.dbo.cwd_directory_operation (directory_id, operation_type) VALUES (131073, 'CREATE_USER');
INSERT INTO confluence5.dbo.cwd_directory_operation (directory_id, operation_type) VALUES (131073, 'DELETE_GROUP');
INSERT INTO confluence5.dbo.cwd_directory_operation (directory_id, operation_type) VALUES (131073, 'DELETE_ROLE');
INSERT INTO confluence5.dbo.cwd_directory_operation (directory_id, operation_type) VALUES (131073, 'DELETE_USER');
INSERT INTO confluence5.dbo.cwd_directory_operation (directory_id, operation_type) VALUES (131073, 'UPDATE_GROUP');
INSERT INTO confluence5.dbo.cwd_directory_operation (directory_id, operation_type) VALUES (131073, 'UPDATE_GROUP_ATTRIBUTE');
INSERT INTO confluence5.dbo.cwd_directory_operation (directory_id, operation_type) VALUES (131073, 'UPDATE_ROLE');
INSERT INTO confluence5.dbo.cwd_directory_operation (directory_id, operation_type) VALUES (131073, 'UPDATE_ROLE_ATTRIBUTE');
INSERT INTO confluence5.dbo.cwd_directory_operation (directory_id, operation_type) VALUES (131073, 'UPDATE_USER');
INSERT INTO confluence5.dbo.cwd_directory_operation (directory_id, operation_type) VALUES (131073, 'UPDATE_USER_ATTRIBUTE');