INSERT INTO confluence5.dbo.BANDANA (BANDANAID, BANDANACONTEXT, BANDANAKEY, BANDANAVALUE) VALUES (1, '_GLOBAL', 'confluence.server.id', '<string>B5DA-4BXW-96BF-DRGC</string>');
INSERT INTO confluence5.dbo.BANDANA (BANDANAID, BANDANACONTEXT, BANDANAKEY, BANDANAVALUE) VALUES (2, '_GLOBAL', 'confluence.server.installation.date', '<date>2017-06-27 14:58:16.197 AEST</date>');
INSERT INTO confluence5.dbo.BANDANA (BANDANAID, BANDANACONTEXT, BANDANAKEY, BANDANAVALUE) VALUES (3, '_GLOBAL', 'atlassian.confluence.settings', '<settings>
  <doNotSave>false</doNotSave>
  <allowCamelCase>false</allowCamelCase>
  <allowTrackbacks>false</allowTrackbacks>
  <allowThreadedComments>true</allowThreadedComments>
  <externalUserManagement>false</externalUserManagement>
  <denyPublicSignup>true</denyPublicSignup>
  <emailAdminMessageOff>false</emailAdminMessageOff>
  <almostSupportPeriodEndMessageOff>false</almostSupportPeriodEndMessageOff>
  <senMissingInLicenseMessageOff>true</senMissingInLicenseMessageOff>
  <baseUrlAdminMessageOff>false</baseUrlAdminMessageOff>
  <allowRemoteApi>false</allowRemoteApi>
  <allowRemoteApiAnonymous>false</allowRemoteApiAnonymous>
  <antiXssMode>true</antiXssMode>
  <gzippingResponse>true</gzippingResponse>
  <disableLogo>false</disableLogo>
  <sharedMode>false</sharedMode>
  <enableDidYouMean>false</enableDidYouMean>
  <enableQuickNav>true</enableQuickNav>
  <enableSpaceStyles>false</enableSpaceStyles>
  <enableOpenSearch>true</enableOpenSearch>
  <showSystemInfoIn500Page>false</showSystemInfoIn500Page>
  <showApplicationTitle>false</showApplicationTitle>
  <referrerSettings>
    <collectReferrerData>true</collectReferrerData>
    <excludedReferrers/>
    <hideExternalReferrers>false</hideExternalReferrers>
  </referrerSettings>
  <captchaSettings>
    <enableCaptcha>false</enableCaptcha>
    <enableDebug>false</enableDebug>
    <captchaGroups class="list"/>
    <exclude>registered</exclude>
  </captchaSettings>
  <customHtmlSettings>
    <beforeHeadEnd></beforeHeadEnd>
    <afterBodyStart></afterBodyStart>
    <beforeBodyEnd></beforeBodyEnd>
  </customHtmlSettings>
  <colourSchemesSettings>
    <colourSchemeType>custom</colourSchemeType>
  </colourSchemesSettings>
  <loginManagerSettings>
    <enableElevatedSecurityCheck>true</enableElevatedSecurityCheck>
    <loginAttemptsThreshold>3</loginAttemptsThreshold>
  </loginManagerSettings>
  <confluenceHttpParameters>
    <connectionTimeout>10000</connectionTimeout>
    <socketTimeout>10000</socketTimeout>
    <enabled>true</enabled>
  </confluenceHttpParameters>
  <attachmentMaxSize>104857600</attachmentMaxSize>
  <auditLogRetentionNumber>3</auditLogRetentionNumber>
  <auditLogRetentionUnit>Years</auditLogRetentionUnit>
  <draftSaveInterval>30000</draftSaveInterval>
  <maxAttachmentsInUI>5</maxAttachmentsInUI>
  <siteTitle>Confluence</siteTitle>
  <documentationUrlPattern>http://docs.atlassian.com/confluence/docs-{0}/{1}</documentationUrlPattern>
  <showContactAdministratorsForm>true</showContactAdministratorsForm>
  <emailAddressVisibility>email.address.public</emailAddressVisibility>
  <defaultEncoding>UTF-8</defaultEncoding>
  <maxThumbHeight>300</maxThumbHeight>
  <maxThumbWidth>300</maxThumbWidth>
  <backupAttachmentsDaily>true</backupAttachmentsDaily>
  <backupDaily>true</backupDaily>
  <backupPath>/Users/chibichii/dev/tomcat/home-cluster-shared/oracle/backups</backupPath>
  <nofollowExternalLinks>true</nofollowExternalLinks>
  <indexingLanguage>english</indexingLanguage>
  <globalDefaultLocale>en_GB</globalDefaultLocale>
  <dailyBackupFilePrefix>backup-</dailyBackupFilePrefix>
  <dailyBackupDateFormatPattern>yyyy_MM_dd</dailyBackupDateFormatPattern>
  <supportRequestEmail>confluence-autosupportrequests@atlassian.com</supportRequestEmail>
  <defaultSpaceHomepageTitle>Home</defaultSpaceHomepageTitle>
  <baseUrl>http://localhost:8090</baseUrl>
  <attachmentDataStore>file.system.based.attachments.storage</attachmentDataStore>
  <displayLinkIcons>false</displayLinkIcons>
  <addWildcardsToUserAndGroupSearches>true</addWildcardsToUserAndGroupSearches>
  <xsrfAddComments>true</xsrfAddComments>
  <webSudoTimeout>10</webSudoTimeout>
  <webSudoEnabled>true</webSudoEnabled>
  <defaultUsersGroup>confluence-users</defaultUsersGroup>
  <attachmentSecurityLevel>smart</attachmentSecurityLevel>
  <enableJavascriptTop>true</enableJavascriptTop>
  <supportPeriodEndMessageOff>false</supportPeriodEndMessageOff>
  <enableWysiwyg>true</enableWysiwyg>
  <useWysiwygByDefault>true</useWysiwygByDefault>
  <numberOfBreadcrumbAncestors>1</numberOfBreadcrumbAncestors>
  <viewSpaceGoesToSpaceSummary>false</viewSpaceGoesToSpaceSummary>
  <maxSimultaneousQuickNavRequests>40</maxSimultaneousQuickNavRequests>
  <maxRssItems>200</maxRssItems>
  <rssTimeout>60</rssTimeout>
  <pageTimeout>120</pageTimeout>
</settings>');
INSERT INTO confluence5.dbo.BANDANA (BANDANAID, BANDANACONTEXT, BANDANAKEY, BANDANAVALUE) VALUES (4, '_GLOBAL', 'synchrony_collaborative_editor_UUID', '<string>14ada6e0-e41c-4c85-8bc1-f0945ff84fa6</string>');
INSERT INTO confluence5.dbo.BANDANA (BANDANAID, BANDANACONTEXT, BANDANAKEY, BANDANAVALUE) VALUES (5, 'com.atlassian.confluence.efi.store.GlobalStorageServiceImpl', 'efi.store.onboarding.plugin-installed-date-in-millis', '<string>1498539504511</string>');
INSERT INTO confluence5.dbo.BANDANA (BANDANAID, BANDANACONTEXT, BANDANAKEY, BANDANAVALUE) VALUES (6, '_GLOBAL', 'synchrony_collaborative_editor_app_passphrase', '<string>R3J8L/qf9pF973kgggPGCB9qq4y3bK3x+vmOBzMy/A4=</string>');
INSERT INTO confluence5.dbo.BANDANA (BANDANAID, BANDANACONTEXT, BANDANAKEY, BANDANAVALUE) VALUES (7, '_GLOBAL', 'synchrony_collaborative_editor_app_id', '<string>Synchrony-2202c546-fab0-350e-a788-f5645f17a0e1</string>');
INSERT INTO confluence5.dbo.BANDANA (BANDANAID, BANDANACONTEXT, BANDANAKEY, BANDANAVALUE) VALUES (8, '_GLOBAL', 'synchrony_collaborative_editor_app_secret', '<string>qrq93IFel9Fks/0HyjeOxP8FzNrqEe/0NHoPCDP41nM=</string>');
INSERT INTO confluence5.dbo.BANDANA (BANDANAID, BANDANACONTEXT, BANDANAKEY, BANDANAVALUE) VALUES (9, '_GLOBAL', 'plugin.manager.state.Map', '<map/>');
INSERT INTO confluence5.dbo.BANDANA (BANDANAID, BANDANACONTEXT, BANDANAKEY, BANDANAVALUE) VALUES (10, '_GLOBAL', 'AO_9412A1_#', '<string>8</string>');
INSERT INTO confluence5.dbo.BANDANA (BANDANAID, BANDANACONTEXT, BANDANAKEY, BANDANAVALUE) VALUES (11, '_GLOBAL', 'atlassian.confluence.plugin.resource.counter', '<int>2</int>');
INSERT INTO confluence5.dbo.BANDANA (BANDANAID, BANDANACONTEXT, BANDANAKEY, BANDANAVALUE) VALUES (12, '_GLOBAL', 'AO_26DB7F_#', '<string>1</string>');
INSERT INTO confluence5.dbo.BANDANA (BANDANAID, BANDANACONTEXT, BANDANAKEY, BANDANAVALUE) VALUES (13, '_GLOBAL', 'com.atlassian.confluence.plugins.confluence-edge-index:build', '<string>1</string>');
INSERT INTO confluence5.dbo.BANDANA (BANDANAID, BANDANACONTEXT, BANDANAKEY, BANDANAVALUE) VALUES (14, '_GLOBAL', 'com.atlassian.confluence.plugins.confluence-healthcheck-plugin:build', '<string>1</string>');
INSERT INTO confluence5.dbo.BANDANA (BANDANAID, BANDANACONTEXT, BANDANAKEY, BANDANAVALUE) VALUES (15, '_GLOBAL', 'com.atlassian.crowd.embedded.admin:build', '<string>3</string>');
INSERT INTO confluence5.dbo.BANDANA (BANDANAID, BANDANACONTEXT, BANDANAKEY, BANDANAVALUE) VALUES (16, '_GLOBAL', 'com.atlassian.plugins.atlassian-whitelist-core-plugin:build', '<string>3</string>');
INSERT INTO confluence5.dbo.BANDANA (BANDANAID, BANDANACONTEXT, BANDANAKEY, BANDANAVALUE) VALUES (17, '_GLOBAL', 'com.atlassian.plugins.custom_apps.hasCustomOrder', '<string>false</string>');
INSERT INTO confluence5.dbo.BANDANA (BANDANAID, BANDANACONTEXT, BANDANAKEY, BANDANAVALUE) VALUES (18, '_GLOBAL', 'com.atlassian.plugins.atlassian-nav-links-plugin:build', '<string>1</string>');
INSERT INTO confluence5.dbo.BANDANA (BANDANAID, BANDANACONTEXT, BANDANAKEY, BANDANAVALUE) VALUES (19, '_GLOBAL', 'com.atlassian.confluence.plugins.confluence-inline-tasks:build', '<string>3</string>');
INSERT INTO confluence5.dbo.BANDANA (BANDANAID, BANDANACONTEXT, BANDANAKEY, BANDANAVALUE) VALUES (20, '_GLOBAL', 'com.atlassian.confluence.plugins.confluence-document-conversion-library:build', '<string>1</string>');
INSERT INTO confluence5.dbo.BANDANA (BANDANAID, BANDANACONTEXT, BANDANAKEY, BANDANAVALUE) VALUES (21, '_GLOBAL', 'confluence.extra.masterdetail:build', '<string>2</string>');
INSERT INTO confluence5.dbo.BANDANA (BANDANAID, BANDANACONTEXT, BANDANAKEY, BANDANAVALUE) VALUES (22, '_GLOBAL', 'com.atlassian.confluence.plugins.confluence-inline-comments:build', '<string>1</string>');
INSERT INTO confluence5.dbo.BANDANA (BANDANAID, BANDANACONTEXT, BANDANAKEY, BANDANAVALUE) VALUES (23, '_GLOBAL', 'com.atlassian.confluence.plugins.confluence-space-ia:build', '<string>1</string>');
INSERT INTO confluence5.dbo.BANDANA (BANDANAID, BANDANACONTEXT, BANDANAKEY, BANDANAVALUE) VALUES (24, '_GLOBAL', 'com.atlassian.confluence.plugins.confluence-collaborative-editor-plugin:build', '<string>1</string>');
INSERT INTO confluence5.dbo.BANDANA (BANDANAID, BANDANACONTEXT, BANDANAKEY, BANDANAVALUE) VALUES (25, '_GLOBAL', 'com.atlassian.confluence.plugins.confluence-roadmap-plugin:build', '<string>6</string>');
INSERT INTO confluence5.dbo.BANDANA (BANDANAID, BANDANACONTEXT, BANDANAKEY, BANDANAVALUE) VALUES (26, '_GLOBAL', 'com.atlassian.upm.atlassian-universal-plugin-manager-plugin:build', '<string>5</string>');
INSERT INTO confluence5.dbo.BANDANA (BANDANAID, BANDANACONTEXT, BANDANAKEY, BANDANAVALUE) VALUES (27, '_GLOBAL', 'com.atlassian.upm.log.PluginSettingsAuditLogService:log:upm_audit_log_v3', '<list>
  <string>{&quot;userKey&quot;:&quot;Confluence&quot;,&quot;date&quot;:1498539840405,&quot;i18nKey&quot;:&quot;upm.auditLog.upm.startup&quot;,&quot;entryType&quot;:&quot;UPM_STARTUP&quot;,&quot;params&quot;:[]}</string>
</list>');
INSERT INTO confluence5.dbo.BANDANA (BANDANAID, BANDANACONTEXT, BANDANAKEY, BANDANAVALUE) VALUES (29, '_GLOBAL', 'com.atlassian.upm:notifications:dismissal-plugin.request', '<list/>');
INSERT INTO confluence5.dbo.BANDANA (BANDANAID, BANDANACONTEXT, BANDANAKEY, BANDANAVALUE) VALUES (30, '_GLOBAL', 'com.atlassian.upm:notifications:dismissal-update', '<list/>');
INSERT INTO confluence5.dbo.BANDANA (BANDANAID, BANDANACONTEXT, BANDANAKEY, BANDANAVALUE) VALUES (31, '_GLOBAL', 'com.atlassian.upm:notifications:dismissal-evaluation.expired', '<list/>');
INSERT INTO confluence5.dbo.BANDANA (BANDANAID, BANDANACONTEXT, BANDANAKEY, BANDANAVALUE) VALUES (33, '_GLOBAL', 'com.atlassian.upm:notifications:dismissal-edition.mismatch', '<list/>');
INSERT INTO confluence5.dbo.BANDANA (BANDANAID, BANDANACONTEXT, BANDANAKEY, BANDANAVALUE) VALUES (34, '_GLOBAL', 'com.atlassian.upm:notifications:dismissal-maintenance.expired', '<list/>');
INSERT INTO confluence5.dbo.BANDANA (BANDANAID, BANDANACONTEXT, BANDANAKEY, BANDANAVALUE) VALUES (36, '_GLOBAL', 'com.atlassian.upm:notifications:dismissal-auto.updated.plugin', '<list/>');
INSERT INTO confluence5.dbo.BANDANA (BANDANAID, BANDANACONTEXT, BANDANAKEY, BANDANAVALUE) VALUES (37, '_GLOBAL', 'com.atlassian.upm:notifications:dismissal-auto.updated.upm', '<list/>');
INSERT INTO confluence5.dbo.BANDANA (BANDANAID, BANDANACONTEXT, BANDANAKEY, BANDANAVALUE) VALUES (38, '_GLOBAL', 'com.atlassian.upm.request.PluginSettingsPluginRequestStore:requests:requests_v2', '<list/>');
INSERT INTO confluence5.dbo.BANDANA (BANDANAID, BANDANACONTEXT, BANDANAKEY, BANDANAVALUE) VALUES (39, '_GLOBAL', 'com.atlassian.support.stp:build', '<string>1</string>');
INSERT INTO confluence5.dbo.BANDANA (BANDANAID, BANDANACONTEXT, BANDANAKEY, BANDANAVALUE) VALUES (40, '_GLOBAL', 'com.atlassian.confluence.plugins.confluence-create-content-plugin:build', '<string>4</string>');
INSERT INTO confluence5.dbo.BANDANA (BANDANAID, BANDANACONTEXT, BANDANAKEY, BANDANAVALUE) VALUES (41, '_GLOBAL', 'com.atlassian.analytics.client.configuration..policy_acknowledged', '<string>true</string>');
INSERT INTO confluence5.dbo.BANDANA (BANDANAID, BANDANACONTEXT, BANDANAKEY, BANDANAVALUE) VALUES (42, '_GLOBAL', 'com.atlassian.analytics.client.configuration.uuid', '<string>7d733cf6-45ff-4f46-9300-eddfb9e71d59</string>');
INSERT INTO confluence5.dbo.BANDANA (BANDANAID, BANDANACONTEXT, BANDANAKEY, BANDANAVALUE) VALUES (43, '_GLOBAL', 'gadget.counter', '<int>2</int>');
INSERT INTO confluence5.dbo.BANDANA (BANDANAID, BANDANACONTEXT, BANDANAKEY, BANDANAVALUE) VALUES (44, '_GLOBAL', 'com.atlassian.analytics.client.configuration..analytics_enabled', '<string>true</string>');
INSERT INTO confluence5.dbo.BANDANA (BANDANAID, BANDANACONTEXT, BANDANAKEY, BANDANAVALUE) VALUES (45, '_GLOBAL', 'com.atlassian.analytics.client.configuration.serverid', '<string>B5DA-4BXW-96BF-DRGC</string>');
INSERT INTO confluence5.dbo.BANDANA (BANDANAID, BANDANACONTEXT, BANDANAKEY, BANDANAVALUE) VALUES (46, '_GLOBAL', 'com.atlassian.upm:notifications:notification-update', '<list/>');
INSERT INTO confluence5.dbo.BANDANA (BANDANAID, BANDANACONTEXT, BANDANAKEY, BANDANAVALUE) VALUES (47, 'com.atlassian.confluence.content.render.xhtml.migration.macro.MacroMigrationService', 'migration.required', '<boolean>false</boolean>');
INSERT INTO confluence5.dbo.BANDANA (BANDANAID, BANDANACONTEXT, BANDANAKEY, BANDANAVALUE) VALUES (48, 'com.atlassian.confluence.schedule.ScheduledJobConfiguration', 'DEFAULT#LocalTaskQueueFlushJob', '<com.atlassian.confluence.schedule.ScheduledJobConfiguration>
  <enabled>
    <value>1</value>
  </enabled>
  <cronSchedule/>
  <repeatInterval/>
</com.atlassian.confluence.schedule.ScheduledJobConfiguration>');
INSERT INTO confluence5.dbo.BANDANA (BANDANAID, BANDANACONTEXT, BANDANAKEY, BANDANAVALUE) VALUES (49, 'com.atlassian.confluence.schedule.ScheduledJobConfiguration', 'DEFAULT#AuditLogCleaner', '<com.atlassian.confluence.schedule.ScheduledJobConfiguration>
  <enabled>
    <value>1</value>
  </enabled>
  <cronSchedule/>
  <repeatInterval/>
</com.atlassian.confluence.schedule.ScheduledJobConfiguration>');
INSERT INTO confluence5.dbo.BANDANA (BANDANAID, BANDANACONTEXT, BANDANAKEY, BANDANAVALUE) VALUES (50, '_GLOBAL', 'com.atlassian.upm:notifications:notification-edition.mismatch', '<list/>');
INSERT INTO confluence5.dbo.BANDANA (BANDANAID, BANDANACONTEXT, BANDANAKEY, BANDANAVALUE) VALUES (51, '_GLOBAL', 'com.atlassian.upm:notifications:notification-evaluation.expired', '<list/>');
INSERT INTO confluence5.dbo.BANDANA (BANDANAID, BANDANACONTEXT, BANDANAKEY, BANDANAVALUE) VALUES (52, 'com.atlassian.confluence.schedule.ScheduledJobConfiguration', 'DEFAULT#CleanTempDirectoryJob', '<com.atlassian.confluence.schedule.ScheduledJobConfiguration>
  <enabled>
    <value>1</value>
  </enabled>
  <cronSchedule/>
  <repeatInterval/>
</com.atlassian.confluence.schedule.ScheduledJobConfiguration>');
INSERT INTO confluence5.dbo.BANDANA (BANDANAID, BANDANACONTEXT, BANDANAKEY, BANDANAVALUE) VALUES (53, '_GLOBAL', 'com.atlassian.upm:notifications:notification-evaluation.nearlyexpired', '<list/>');
INSERT INTO confluence5.dbo.BANDANA (BANDANAID, BANDANACONTEXT, BANDANAKEY, BANDANAVALUE) VALUES (54, 'com.atlassian.confluence.schedule.ScheduledJobConfiguration', 'DEFAULT#EhCacheCompactionJob', '<com.atlassian.confluence.schedule.ScheduledJobConfiguration>
  <enabled>
    <value>1</value>
  </enabled>
  <cronSchedule/>
  <repeatInterval/>
</com.atlassian.confluence.schedule.ScheduledJobConfiguration>');
INSERT INTO confluence5.dbo.BANDANA (BANDANAID, BANDANACONTEXT, BANDANAKEY, BANDANAVALUE) VALUES (55, '_GLOBAL', 'com.atlassian.upm:notifications:notification-maintenance.expired', '<list/>');
INSERT INTO confluence5.dbo.BANDANA (BANDANAID, BANDANACONTEXT, BANDANAKEY, BANDANAVALUE) VALUES (56, '_GLOBAL', 'com.atlassian.analytics.client.configuration..logged_base_analytics_data', '<string>true</string>');
INSERT INTO confluence5.dbo.BANDANA (BANDANAID, BANDANACONTEXT, BANDANAKEY, BANDANAVALUE) VALUES (57, 'com.atlassian.confluence.schedule.ScheduledJobConfiguration', 'DEFAULT#TaskQueueFlushJob', '<com.atlassian.confluence.schedule.ScheduledJobConfiguration>
  <enabled>
    <value>1</value>
  </enabled>
  <cronSchedule/>
  <repeatInterval/>
</com.atlassian.confluence.schedule.ScheduledJobConfiguration>');
INSERT INTO confluence5.dbo.BANDANA (BANDANAID, BANDANACONTEXT, BANDANAKEY, BANDANAVALUE) VALUES (58, '_GLOBAL', 'com.atlassian.upm:notifications:notification-maintenance.nearlyexpired', '<list/>');
INSERT INTO confluence5.dbo.BANDANA (BANDANAID, BANDANACONTEXT, BANDANAKEY, BANDANAVALUE) VALUES (59, 'com.atlassian.confluence.schedule.ScheduledJobConfiguration', 'DEFAULT#ClusterCacheCompactionJob', '<com.atlassian.confluence.schedule.ScheduledJobConfiguration>
  <enabled>
    <value>1</value>
  </enabled>
  <cronSchedule/>
  <repeatInterval/>
</com.atlassian.confluence.schedule.ScheduledJobConfiguration>');
INSERT INTO confluence5.dbo.BANDANA (BANDANAID, BANDANACONTEXT, BANDANAKEY, BANDANAVALUE) VALUES (60, '_GLOBAL', 'com.atlassian.upm:notifications:notification-plugin.request', '<list/>');
INSERT INTO confluence5.dbo.BANDANA (BANDANAID, BANDANACONTEXT, BANDANAKEY, BANDANAVALUE) VALUES (61, 'com.atlassian.confluence.schedule.ScheduledJobConfiguration', 'DEFAULT#LaasPerformanceLoggingJob', '<com.atlassian.confluence.schedule.ScheduledJobConfiguration>
  <enabled>
    <value>1</value>
  </enabled>
  <cronSchedule/>
  <repeatInterval/>
</com.atlassian.confluence.schedule.ScheduledJobConfiguration>');
INSERT INTO confluence5.dbo.BANDANA (BANDANAID, BANDANACONTEXT, BANDANAKEY, BANDANAVALUE) VALUES (62, 'com.atlassian.confluence.schedule.ScheduledJobConfiguration', 'DEFAULT#ClusterSafetyJob', '<com.atlassian.confluence.schedule.ScheduledJobConfiguration>
  <enabled>
    <value>1</value>
  </enabled>
  <cronSchedule/>
  <repeatInterval/>
</com.atlassian.confluence.schedule.ScheduledJobConfiguration>');
INSERT INTO confluence5.dbo.BANDANA (BANDANAID, BANDANACONTEXT, BANDANAKEY, BANDANAVALUE) VALUES (63, 'com.atlassian.confluence.schedule.ScheduledJobConfiguration', 'DEFAULT#ClearExpiredRememberMeTokensJob', '<com.atlassian.confluence.schedule.ScheduledJobConfiguration>
  <enabled>
    <value>1</value>
  </enabled>
  <cronSchedule/>
  <repeatInterval/>
</com.atlassian.confluence.schedule.ScheduledJobConfiguration>');
INSERT INTO confluence5.dbo.BANDANA (BANDANAID, BANDANACONTEXT, BANDANAKEY, BANDANAVALUE) VALUES (64, 'com.atlassian.confluence.schedule.ScheduledJobConfiguration', 'DEFAULT#JournalCleaner', '<com.atlassian.confluence.schedule.ScheduledJobConfiguration>
  <enabled>
    <value>1</value>
  </enabled>
  <cronSchedule/>
  <repeatInterval/>
</com.atlassian.confluence.schedule.ScheduledJobConfiguration>');
INSERT INTO confluence5.dbo.BANDANA (BANDANAID, BANDANACONTEXT, BANDANAKEY, BANDANAVALUE) VALUES (65, 'com.atlassian.confluence.schedule.ScheduledJobConfiguration', 'DEFAULT#MailQueueFlushJob', '<com.atlassian.confluence.schedule.ScheduledJobConfiguration>
  <enabled>
    <value>1</value>
  </enabled>
  <cronSchedule/>
  <repeatInterval/>
</com.atlassian.confluence.schedule.ScheduledJobConfiguration>');
INSERT INTO confluence5.dbo.BANDANA (BANDANAID, BANDANACONTEXT, BANDANAKEY, BANDANAVALUE) VALUES (66, 'com.atlassian.confluence.schedule.ScheduledJobConfiguration', 'DEFAULT#clearOldMailErrorsJob', '<com.atlassian.confluence.schedule.ScheduledJobConfiguration>
  <enabled>
    <value>1</value>
  </enabled>
  <cronSchedule/>
  <repeatInterval/>
</com.atlassian.confluence.schedule.ScheduledJobConfiguration>');
INSERT INTO confluence5.dbo.BANDANA (BANDANAID, BANDANACONTEXT, BANDANAKEY, BANDANAVALUE) VALUES (67, 'com.atlassian.confluence.schedule.ScheduledJobConfiguration', 'DEFAULT#DeferredFileDeletionJob', '<com.atlassian.confluence.schedule.ScheduledJobConfiguration>
  <enabled>
    <value>1</value>
  </enabled>
  <cronSchedule/>
  <repeatInterval/>
</com.atlassian.confluence.schedule.ScheduledJobConfiguration>');
INSERT INTO confluence5.dbo.BANDANA (BANDANAID, BANDANACONTEXT, BANDANAKEY, BANDANAVALUE) VALUES (68, 'com.atlassian.confluence.schedule.ScheduledJobConfiguration', 'DEFAULT#BackupJob', '<com.atlassian.confluence.schedule.ScheduledJobConfiguration>
  <enabled>
    <value>1</value>
  </enabled>
  <cronSchedule/>
  <repeatInterval/>
</com.atlassian.confluence.schedule.ScheduledJobConfiguration>');
INSERT INTO confluence5.dbo.BANDANA (BANDANAID, BANDANACONTEXT, BANDANAKEY, BANDANAVALUE) VALUES (69, 'com.atlassian.confluence.schedule.ScheduledJobConfiguration', 'DEFAULT#DailyReportJob', '<com.atlassian.confluence.schedule.ScheduledJobConfiguration>
  <enabled>
    <value>1</value>
  </enabled>
  <cronSchedule/>
  <repeatInterval/>
</com.atlassian.confluence.schedule.ScheduledJobConfiguration>');
INSERT INTO confluence5.dbo.BANDANA (BANDANAID, BANDANACONTEXT, BANDANAKEY, BANDANAVALUE) VALUES (70, 'com.atlassian.confluence.schedule.ScheduledJobConfiguration', 'DEFAULT#ESIndexJournalVerifierJob', '<com.atlassian.confluence.schedule.ScheduledJobConfiguration>
  <enabled>
    <value>1</value>
  </enabled>
  <cronSchedule/>
  <repeatInterval/>
</com.atlassian.confluence.schedule.ScheduledJobConfiguration>');
INSERT INTO confluence5.dbo.BANDANA (BANDANAID, BANDANACONTEXT, BANDANAKEY, BANDANAVALUE) VALUES (71, 'com.atlassian.confluence.schedule.ScheduledJobConfiguration', 'DEFAULT#batchingJobConfig', '<com.atlassian.confluence.schedule.ScheduledJobConfiguration>
  <enabled>
    <value>1</value>
  </enabled>
  <cronSchedule/>
  <repeatInterval/>
</com.atlassian.confluence.schedule.ScheduledJobConfiguration>');
INSERT INTO confluence5.dbo.BANDANA (BANDANAID, BANDANACONTEXT, BANDANAKEY, BANDANAVALUE) VALUES (72, 'com.atlassian.confluence.schedule.ScheduledJobConfiguration', 'com.atlassian.confluence.plugins.confluence-onboarding#onboardingSpaceCheckJob', '<com.atlassian.confluence.schedule.ScheduledJobConfiguration>
  <enabled>
    <value>1</value>
  </enabled>
  <cronSchedule/>
  <repeatInterval/>
</com.atlassian.confluence.schedule.ScheduledJobConfiguration>');
INSERT INTO confluence5.dbo.BANDANA (BANDANAID, BANDANACONTEXT, BANDANAKEY, BANDANAVALUE) VALUES (73, 'com.atlassian.confluence.schedule.ScheduledJobConfiguration', 'com.atlassian.confluence.plugins.confluence-daily-summary-email#summaryEmail', '<com.atlassian.confluence.schedule.ScheduledJobConfiguration>
  <enabled>
    <value>1</value>
  </enabled>
  <cronSchedule/>
  <repeatInterval/>
</com.atlassian.confluence.schedule.ScheduledJobConfiguration>');
INSERT INTO confluence5.dbo.BANDANA (BANDANAID, BANDANACONTEXT, BANDANAKEY, BANDANAVALUE) VALUES (74, 'com.atlassian.confluence.schedule.ScheduledJobConfiguration', 'com.atlassian.confluence.plugins.recently-viewed-plugin#purgeHistoryJob', '<com.atlassian.confluence.schedule.ScheduledJobConfiguration>
  <enabled>
    <value>1</value>
  </enabled>
  <cronSchedule/>
  <repeatInterval/>
</com.atlassian.confluence.schedule.ScheduledJobConfiguration>');
INSERT INTO confluence5.dbo.BANDANA (BANDANAID, BANDANACONTEXT, BANDANAKEY, BANDANAVALUE) VALUES (75, 'com.atlassian.confluence.schedule.ScheduledJobConfiguration', 'DEFAULT#flushEdgeIndexQueue', '<com.atlassian.confluence.schedule.ScheduledJobConfiguration>
  <enabled>
    <value>1</value>
  </enabled>
  <cronSchedule/>
  <repeatInterval/>
</com.atlassian.confluence.schedule.ScheduledJobConfiguration>');
INSERT INTO confluence5.dbo.BANDANA (BANDANAID, BANDANACONTEXT, BANDANAKEY, BANDANAVALUE) VALUES (76, 'com.atlassian.confluence.schedule.ScheduledJobConfiguration', 'com.atlassian.confluence.plugins.confluence-jira-metadata#jira-metadata-cache-config', '<com.atlassian.confluence.schedule.ScheduledJobConfiguration>
  <enabled>
    <value>1</value>
  </enabled>
  <cronSchedule/>
  <repeatInterval/>
</com.atlassian.confluence.schedule.ScheduledJobConfiguration>');
INSERT INTO confluence5.dbo.BANDANA (BANDANAID, BANDANACONTEXT, BANDANAKEY, BANDANAVALUE) VALUES (77, 'com.atlassian.confluence.schedule.ScheduledJobConfiguration', 'DEFAULT#conversionQueueMonitor', '<com.atlassian.confluence.schedule.ScheduledJobConfiguration>
  <enabled>
    <value>1</value>
  </enabled>
  <cronSchedule/>
  <repeatInterval/>
</com.atlassian.confluence.schedule.ScheduledJobConfiguration>');
INSERT INTO confluence5.dbo.BANDANA (BANDANAID, BANDANACONTEXT, BANDANAKEY, BANDANAVALUE) VALUES (78, 'com.atlassian.confluence.schedule.ScheduledJobConfiguration', 'com.atlassian.confluence.plugins.confluence-onboarding#onboardingNumberOfUsersCheckJob', '<com.atlassian.confluence.schedule.ScheduledJobConfiguration>
  <enabled>
    <value>1</value>
  </enabled>
  <cronSchedule/>
  <repeatInterval/>
</com.atlassian.confluence.schedule.ScheduledJobConfiguration>');
INSERT INTO confluence5.dbo.BANDANA (BANDANAID, BANDANACONTEXT, BANDANAKEY, BANDANAVALUE) VALUES (79, 'com.atlassian.confluence.schedule.ScheduledJobConfiguration', 'DEFAULT#periodicEventPublisherJob', '<com.atlassian.confluence.schedule.ScheduledJobConfiguration>
  <enabled>
    <value>1</value>
  </enabled>
  <cronSchedule/>
  <repeatInterval/>
</com.atlassian.confluence.schedule.ScheduledJobConfiguration>');
INSERT INTO confluence5.dbo.BANDANA (BANDANAID, BANDANACONTEXT, BANDANAKEY, BANDANAVALUE) VALUES (80, 'com.atlassian.confluence.efi.store.GlobalStorageServiceImpl', 'efi.store.onboarding.onboardingSpaceCheckJob', '<string>JOB_FIRST_EXECUTE</string>');
INSERT INTO confluence5.dbo.BANDANA (BANDANAID, BANDANACONTEXT, BANDANAKEY, BANDANAVALUE) VALUES (81, 'com.atlassian.confluence.efi.store.GlobalStorageServiceImpl', 'efi.store.onboarding.onboardingNumberOfUsersCheckJob', '<string>JOB_FIRST_EXECUTE</string>');
INSERT INTO confluence5.dbo.BANDANA (BANDANAID, BANDANACONTEXT, BANDANAKEY, BANDANAVALUE) VALUES (82, '_GLOBAL', 'plugin.settings.com.atlassian.confluence.bigpipe.BigPipeSettingsManager', '<com.atlassian.confluence.internal.bigpipe.BigPipeSettingsManager-BigPipeSettings>
  <pluginSettings>
    <entry>
      <string>com.atlassian.confluence.plugins.confluence-content-report-plugin:content-report-table</string>
      <boolean>false</boolean>
    </entry>
    <entry>
      <string>com.gliffy.integration.confluence:gliffy</string>
      <boolean>true</boolean>
    </entry>
    <entry>
      <string>com.atlassian.confluence.plugins.confluence-inline-tasks:tasks-report-macro</string>
      <boolean>false</boolean>
    </entry>
    <entry>
      <string>confluence.macros.advanced:blog-posts</string>
      <boolean>false</boolean>
    </entry>
    <entry>
      <string>confluence.macros.advanced:children</string>
      <boolean>true</boolean>
    </entry>
    <entry>
      <string>confluence.extra.jira:jira</string>
      <boolean>true</boolean>
    </entry>
    <entry>
      <string>confluence.extra.jira:jirachart</string>
      <boolean>true</boolean>
    </entry>
    <entry>
      <string>confluence.macros.advanced:include</string>
      <boolean>false</boolean>
    </entry>
    <entry>
      <string>confluence.macros.advanced:excerpt-include</string>
      <boolean>false</boolean>
    </entry>
    <entry>
      <string>confluence.extra.chart:chart</string>
      <boolean>true</boolean>
    </entry>
    <entry>
      <string>org.randombits.confluence.toc:toc</string>
      <boolean>false</boolean>
    </entry>
    <entry>
      <string>confluence.extra.masterdetail:detailssummary</string>
      <boolean>true</boolean>
    </entry>
    <entry>
      <string>com.atlassian.confluence.contributors:contributors</string>
      <boolean>false</boolean>
    </entry>
    <entry>
      <string>com.atlassian.confluence.contributors:contributors-summary</string>
      <boolean>false</boolean>
    </entry>
    <entry>
      <string>confluence.macros.advanced:recently-updated</string>
      <boolean>true</boolean>
    </entry>
    <entry>
      <string>confluence.extra.jira:jiraissues</string>
      <boolean>false</boolean>
    </entry>
  </pluginSettings>
</com.atlassian.confluence.internal.bigpipe.BigPipeSettingsManager-BigPipeSettings>');